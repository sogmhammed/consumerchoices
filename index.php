<?php 
	
	class CreditsParser{

		public $credits;
		public $movieIds = [];
		public $movieTitles = [];
		public $movieCasts = [];
		public $movieCrews = [];
		public $actors = [];
		public $actorsOutput = [];
		public $listOfCharacters = [];

		function getCreditsData()
		{
			foreach($this->credits as $rowIn => $row):
				$row = explode(',', $row);
				if($rowIn > 0){
					foreach($row as $colIn => $column):
						if($colIn === 0){
							array_push($this->movieIds, $column);
						}else if($colIn === 1){
							array_push($this->movieTitles, $column);
						}
					endforeach;

					//remove id and title
					array_shift($row);
					array_shift($row);

					//fix csv data to enable correct json decoding;
					$row = implode(',', $row);
					$row = substr(substr(str_replace("\"\"", "\"", $row), 1), 0, -2);
					$row = str_replace("\\\"", "", $row);
					$row = explode('}]', $row);
					$casts = $row[0]."}]";
					$crews = isset($row[1]) ? substr($row[1], 3)."}]" : '';

					//decode and save data
					array_push($this->movieCasts, json_decode($casts));
					array_push($this->movieCrews, json_decode($crews));
				}

			endforeach;
		}

		function getActors()
		{
			foreach($this->movieCasts as $mI => $mCast):
				if(isset($mCast)):
					foreach($mCast as $castObj){
						if(!isset($this->actors[$castObj->id])):

							$castData = array(
								"id" => $castObj->id,
								"name" => $castObj->name
							);

							$this->actors[$castObj->id] = $castData;

							array_push($this->actorsOutput, $castData);
						endif;

						$this->listOfCharacters[$mI][] = array(
							"id" => $castObj->cast_id,
							"character_name" => $castObj->character
						);
					}
				endif;
			endforeach;
		}

		function saveActors()
		{
			$this->getActors();
			file_put_contents('output/actors.json', json_encode($this->actorsOutput, JSON_PRETTY_PRINT));
		}
	}


	class MoviesParser extends CreditsParser{

		public $movies;
		public $genres;
		public $keywords;
		public $actors;
		public $genresOutput = [];
		public $keywordsOutput = [];
		public $listOfGenreIds = [];
		public $listOfKeywordIds = [];
		public $listOfBudgets = [];
		public $listOfMovies = [];

		function __construct()
		{
			$this->movies = array_map('str_getcsv', file('input/Movies.csv'));
			$this->credits = file('input/Credits.csv');
		}

		public function getMoviesData()
		{
			foreach($this->movies as $k => $m):
				if($k > 0){	
					$this->listOfBudgets[$k] = $m[0];
					$this->listOfGenreIds[$k] = $this->getGenres($m[1]);
					$this->listOfKeywordIds[$k] = $this->getKeywords($m[4]);
				}
			endforeach;
		}

		public function getGenres($data)
		{
			return $this->extractData($data, 'genres');
		}

		public function getKeywords($data)
		{
			return $this->extractData($data, 'keywords');
		}

		//returns list of Ids
		public function extractData($data, $key)
		{
			$listOfIds = [];
			$dataJson = json_decode($data);
			if(isset($dataJson)){
				foreach($dataJson as $dataObj):
					if(!isset($this->$key[$dataObj->id])){
						$this->$key[$dataObj->id] = $dataObj;
						$outputKey = $key."Output";
						array_push($this->$outputKey, $dataObj);
					}
					array_push($listOfIds, $dataObj->id);
				endforeach;
			}

			return $listOfIds;
		}

		public function saveMoviesData()
		{
			file_put_contents('output/genres.json', json_encode($this->genresOutput, JSON_PRETTY_PRINT));
			file_put_contents('output/keywords.json', json_encode($this->keywordsOutput, JSON_PRETTY_PRINT));
		}

		public function printMoviesJson()
		{
			foreach($this->movieTitles as $movieIndex => $movieTitle):
				$movieData = array(
					"Title" => $movieTitle,
					"Budget" => $this->listOfBudgets[$movieIndex+1],
					"Keyword IDs" => implode(',',$this->listOfKeywordIds[$movieIndex+1]),
					"Genre IDs" => implode(',',$this->listOfGenreIds[$movieIndex+1]),
					"Character" => $this->listOfCharacters[$movieIndex] ?? []
				);
				array_push($this->listOfMovies, $movieData);
			endforeach;

			file_put_contents("output/movies.json", json_encode($this->listOfMovies, JSON_PRETTY_PRINT));

		}
	}


	$movieParser = new MoviesParser();
	//step 1
	$movieParser->getMoviesData();
	$movieParser->saveMoviesData();
	$movieParser->getCreditsData();
	$movieParser->saveActors();
	//step 2
	$movieParser->printMoviesJson();

	echo "Please check output folder for generated json files";
?>